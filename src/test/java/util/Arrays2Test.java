package util;

import org.junit.Test;

import static java.util.stream.IntStream.range;
import static org.junit.Assert.*;

/**
 * Created by eugene on 26/07/15.
 */
public class Arrays2Test {

    @Test
    public void testDeepCopy() throws Exception {
        Object[][] array1 = {};
        verifyDeepCopy(array1);

        Object[][] array2 = {{}};
        verifyDeepCopy(array2);

        Object[][] array3 = {{1}};
        verifyDeepCopy(array3);

        Object[][] array4 = {{1, 2}, {3, 4}};
        verifyDeepCopy(array4);
    }

    private <T> void verifyDeepCopy(T[][] array) {
        T[][] arrayCopy = Arrays2.deepCopyOf(array);
        assertArrayEquals(array, arrayCopy);
        assertNotSame(array, arrayCopy);
        assertTrue(
                range(0, array.length)
                        .mapToObj(idx -> array[idx] != arrayCopy[idx])
                        .allMatch(x -> x)
        );
    }

}