package util;

import static java.util.Arrays.*;

public interface Arrays2 {

    static void swap(Object[] arr, int i, int j) {
        Object temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }

    static void swap(int[] arr, int i, int j) {
        int temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }

    static <T> T[][] deepCopyOf(T[][] matrix) {
        //noinspection Convert2MethodRef
        return stream(matrix).map(el -> el.clone()).toArray($ -> matrix.clone());
    }

    static byte[][] deepCopyOf(byte[][] matrix) {
        return stream(matrix).map(byte[]::clone).toArray($ -> matrix.clone());
    }

    static short[][] deepCopyOf(short[][] matrix) {
        return stream(matrix).map(short[]::clone).toArray($ -> matrix.clone());
    }

    static int[][] deepCopyOf(int[][] matrix) {
        return stream(matrix).map(int[]::clone).toArray($ -> matrix.clone());
    }

    static long[][] deepCopyOf(long[][] matrix) {
        return stream(matrix).map(long[]::clone).toArray($ -> matrix.clone());
    }

    static float[][] deepCopyOf(float[][] matrix) {
        return stream(matrix).map(float[]::clone).toArray($ -> matrix.clone());
    }

    static double[][] deepCopyOf(double[][] matrix) {
        return stream(matrix).map(double[]::clone).toArray($ -> matrix.clone());
    }

}
