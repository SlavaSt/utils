package util;

import com.google.common.base.Throwables;

import java.util.concurrent.Callable;

/**
 * Created by levineugene on 12/3/14.
 */
public interface Commons {

    static <T> T ignoreExceptions(Callable<T> callable) {
        try {
            return callable.call();
        } catch (Exception e) {
            throw Throwables.propagate(e);
        }
    }
}
