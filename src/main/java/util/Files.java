package util;

import java.io.File;

import static util.Commons.ignoreExceptions;

/**
 * Created by levineugene on 12/3/14.
 */
public interface Files {

    static String mkString(File file) {
        return ignoreExceptions(() -> new String(java.nio.file.Files.readAllBytes(file.toPath())));
    }

}
