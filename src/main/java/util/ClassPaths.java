package util;

import java.io.InputStream;
import static com.google.common.io.Resources.getResource;
import static util.Commons.ignoreExceptions;

/**
 * Created by levineugene on 12/3/14.
 */
public interface ClassPaths {

    static InputStream getInputStream(String resourceName) {
        return ClassPaths.class.getResourceAsStream(resourceName);
    }

    static RichFile getFile(String fileName) {
        return ignoreExceptions(() -> new RichFile(getResource(fileName).toURI()));
    }
}
