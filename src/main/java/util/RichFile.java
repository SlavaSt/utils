package util;

import com.google.common.annotations.Beta;

import java.io.File;
import java.net.URI;

/**
 * Created by levineugene on 12/3/14.
 */
public class RichFile extends File {

    // Standard file constructors

    public RichFile(String pathname) {
        super(pathname);
    }

    public RichFile(String parent, String child) {
        super(parent, child);
    }

    public RichFile(File parent, String child) {
        super(parent, child);
    }

    public RichFile(URI uri) {
        super(uri);
    }

    // Wraps a file to add convenience methods

    @Beta
    public RichFile (File file) {
        super(file.toURI()); // TODO make sure it is sound
    }

    public String mkString() {
        return Files.mkString(this);
    }

}
